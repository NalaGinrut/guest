;;  Copyright (C) 2019,2020
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language es10 parser)
  #:use-module (language es10 utils)
  #:use-module (language es10 literals string-numeric)
  #:use-module (system base lalr)
  #:use-module (ice-9 match)
  #:export (read-es10))

(define (read-es10 port)
  (make-reader make-parser make-lua-tokenizer port))

;; NOTE:
;; This LALR(1) spec was inspired by Matt Wette:
;; http://git.savannah.nongnu.org/cgit/nyacc.git/tree/examples/nyacc/lang/javascript/mach.scm#n50
;; NOTE:
;; This spec should abid to ECMA262:
;; https://www.ecma-international.org/ecma-262
(define (make-es10-parser)
  (lalr-parser
   (left: semi-colon comma rparen rbrace rbracket)
   (nonassoc: return true false null)
   (right: then else)
   (right: expr stmt)

   ;; reserved words
   abstract boolean byte char class const debugger double enum export
   extends final float goto implements import int interface long
   native package private protected public short static super
   synchronized throws transient volatile yield yield* await this if

   ;; operations
   or and lt leq gt geq eq neq add minus multi div mod

   ;; literals
   string numeric null boolean

   ;; misc
   id

   (Program ())

   (Literal (null) : `(NullLiteral ,$1)
            (boolean) : `(BooleanLiteral ,$1)
            (numeric) : (if (is-string-numeric-literal? o)
                            `(StringNumericLiteral ,o)
                            `(NumericLiteral ,o))
            (string) : `(StringLiteral ,$1))

   (Identifier (id) : `(Identifier ,$1))

   ;; 12.2 Primary Expression
   (PrimaryExpression
    (this) : #:this
    (Identifier) : $1
    (Literal) : $1
    (ArrayLiteral) : $1
    (ObjectLiteral) : $1
    (FunctionExpression) : $1
    (ClassExpression) : $1
    (GeneratorExpression) : $1
    (AsyncFunctionExpression) : $1
    (AsyncGeneratorExpression) : $1
    (RegularExpressionLiteral) : $1
    (TemplateLiteral) : $1
    (CoverParenthesizedExpressionAndArrowParameterList) : $1)

   (CoverParenthesizedExpressionAndArrowParameterList
    (ParenthsizedExpression) : $1
    (Expression comma) : $1
    () : 'null-expr
    (BindingIdentifier) : $1
    (BindingPattern) : $1
    (Expression comma BindingIdentifier) : (fold-complex-bind-expr $1 $3)
    (Expression comma BindingPattern) : (fold-complex-bind-expr $1 $3))

   (ParenthsizedExpression
    (lparen Expression rparen) : $2)

   (Yield (yield) : $1
          (yield*) : $1)

   ;; 12.1 Identifier
   ;; NOTE:
   ;; yield and await are permitted as BindingIdentifier in the grammar.
   (BindingIdentifier
    (Identifier) : (make-id-bind-node $1)
    (Yield Identifier) : (make-id-bind-node $2 #:mode $1)
    (await Identifier) : (make-id-bind-node $2 #:mode 'await))

   (IdentifierReference
    (Identifier) : (make-id-ref-node $1)
    (Yield Identifier) : (make-id-ref-node $2 #:mode $1)
    (await Identifier) : (make-id-ref-node $2 #:mode 'await))

   (LabelIdentifier
    (Identifier) : (make-id-label-node $1)
    (Yield Identifier) : (make-id-label-node $2 #:mode $1)
    (await Identifier) : (make-id-label-node $2 #:mode 'await))

   (YieldExpression
    (Yield AssignmentExpression) : (make-yield-context $1 $2))

   (IfStatement
    (if lbrace Expression rbrace Statement else Statement)
    : `(if ,$3 ,(validate-branch-statement $5) ,(validate-branch-statement $7))
    (if lbrace Expression rbrace Statement)
    : `(if $3 ,(validate-branch-statement $5)))

   (SwitchStatement
    )

   ))
