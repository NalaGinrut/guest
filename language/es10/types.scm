;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language es10 types)
  #:use-module (language es10 irregex)
  #:use-module (language es10 utils)
  #:use-module (ice-9 match)
  #:use-module ((rnrs) #:select (define-record-type))
  #:export (unique-type
            create-numeric))

(define boolean-true (make-<boolean> 'true))
(define boolean-false (make-<boolean> 'false))
(define null (make-<null>))

(define (unique-type t)
  (match t
    ('true boolean-true)
    ('false boolean-false)
    ('null null)
    (else (es10-error unique-type "Invalid type `~a'!" t))))
