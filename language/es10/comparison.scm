;;  Copyright (C) 2020
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language es10 comparison)
  #:use-module (language es10 utils)
  #:use-module (language es10 prologue)
  #:use-module (ice-9 match)
  #:export ())

(define (absAbstract-relational-comparison x y)
  )

(define (abstract-equality-comparison x y)
  )

(define (strict-equality-comparison x y)
  )
