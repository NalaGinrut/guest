;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language es10 literals string)
  #:use-module (language es10 irregex)
  #:use-module (language es10 utils)
  #:use-module (language es10 types)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:export (is-string-begin?
            read-js-string
            detect-string-type))

(define (is-string-begin? ch)
  (memv ch '(#\" #\' #\`)))

(define (read-js-string quote port)
  (let lp ((next (peek-char port)) (ret '()))
    (cond
     ((char=? quote next)
      (read-char port)
      (list->string (reverse ret)))
     ((char=? next #\\) ; line continuation
      (read-char port) ; skip #\\
      (cond
       ((is-line-terminator? (peek-char port))
        (read-char port) ; skip line terminator
        (lp (peek-char port) ret))
       (else (es10-error read-js-string
                         "SyntaxError:
In single or double quoted string, \
\\ should follow a line terminator.
Otherwise you should use TemplateString."))))
     ((is-line-terminator? next)
      (cond
       ((char=? quote #\`)
        (let ((ch (read-char port)))
          (lp (peek-char port) (cons ch ret))))
       (else (es10-error read-js-string
                         "SyntaxError:
Try TemplateString to embed line terminator."))))
     (else
      (let ((ch (read-char port)))
        (lp (peek-char port) ch))))))

(define (detect-string-type ch)
  (match ch
    (#\" 'DoubleStringCharacters)
    (#\' 'SingleStringCharacters)
    (#\` 'TemplateString)
    (else (es10-error detect-string-type
                      "Invalid string header `~a'!" ch))))
