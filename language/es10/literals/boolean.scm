;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language es10 literals boolean)
  #:use-module (language es10 utils)
  #:use-module (language es10 irregex)
  #:use-module (language es10 types)
  #:use-module (ice-9 match)
  #:export (js-obj->boolean))

;; https://www.ecma-international.org/ecma-262/#sec-toboolean
(define (js-obj->boolean obj)
  (match obj
    (($ <undefined>) (unique-type 'false))
    (($ <null>) (unique-type 'false))
    (($ <boolean>) obj)
    (($ <number>) (cond
                   ((or (zero? obj) (nan? obj)) (unique-type 'false))
                   (else (unique-type 'true))))
    (($ <string> _ val) (if (zero? (string-length val))
                            (unique-type 'false)
                            (unique-type 'true)))
    (($ <symbol>) (unique-type 'true))
    (($ <object>) (unique-type 'true))
    (else (es10-error js-obj->boolean "Invalid object `~a'!" obj))))
