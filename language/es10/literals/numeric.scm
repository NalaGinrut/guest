;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language es10 numeric)
  #:use-module (language es10 irregex)
  #:use-module (language es10 utils)
  #:use-module (language es10 types)
  #:use-module (ice-9 match)
  #:export (js-obj->number))


Argument Type   Result
Undefined       Return NaN.
Null    Return +0.
Boolean If argument is true, return 1. If argument is false, return +0.
Number  Return argument (no conversion).
String  See grammar and conversion algorithm below.
Symbol  Throw a TypeError exception.
Object
Apply the following steps:

Let primValue be ? ToPrimitive(argument, hint Number).
Return ? ToNumber(primValue).
(define (js-obj->number obj)
  (match obj
    (($ <undefined>) (unique-type 'NaN))
    (($ <null>) (create-number +0))
    (($ <boolean>) (if (js-obj-is-true? obj)
                       (create-numeric 1)
                       (create-numeric +0)))
    (($ <number>) obj)
    (($ <string> _ val)
     (let ((str (string-trim-both val)))
       (cond
        ;; string only contains whitespaces ==> +0
        ((string-null? str) (create-numeric +0))

        )
       ))
    (($ <symbol>) (throw 'es10-type-error))
    (($ <object> _ val))
    ))
