;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language es10 string-numeric)
  #:use-module (language es10 irregex)
  #:use-module (language es10 utils)
  #:use-module (ice-9 match)
  #:export ())

(define (js-obj->numeric ))

(define (is-infinity? str)
  (irregex-match "[+-]?Infinity" str))

#|
https://www.ecma-international.org/ecma-262/#sec-tonumber-applied-to-the-string-type

If the grammar cannot interpret the String as an expansion of StringNumericLiteral, then the result of ToNumber is NaN.

|#
(define (is-string-numeric-literal? token-value)
  (when (not (string? token-value))
    (es10-error "Wrong object! `~a' is not a string!" o))
  (let ((str (string-trim-both token-value)))
    (cond
     ((is-infinity? str) #t)
     ((or (string-null? str) (string->number str)) #t)
     (else #f))))
